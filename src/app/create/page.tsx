"use client";

import FormPost from "@/components/FormPost";
import BackButton from "@/components/BackButton";
import { FormInputPost } from "@/types";
import { SubmitHandler } from "react-hook-form";
import { useQueryClient, useMutation } from "@tanstack/react-query";
import axios from "axios";
import { error } from "console";
import { useRouter } from "next/navigation";

const CreatePage = () => {
  const router = useRouter();
  const queryClient = useQueryClient();
  const handleCreatePost: SubmitHandler<FormInputPost> = (data) => {
    createPost(data);
  };

  const { mutate: createPost, isLoading: isLoadingSubmit } = useMutation({
    mutationFn: (newPost: FormInputPost) => {
      return axios.post("/api/posts", newPost);
    },
    onError: (error) => {
      console.error(error);
    },
    onSuccess: () => {
      queryClient.invalidateQueries(['posts']); // Utilisez un tableau de chaînes
      router.push("/");
      router.refresh();
    },
  });
  return (
    <div>
      <BackButton />
      <h1 className="text-2xl my-4 font-bold text-center">Add new post</h1>
      <FormPost
        submit={handleCreatePost}
        isEditing={false}
        isLoadingSubmit={isLoadingSubmit}
      />
    </div>
  );
};

export default CreatePage;
