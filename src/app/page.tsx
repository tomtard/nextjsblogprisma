"use client";

import PostCard from "@/components/PostCard";
import axios from "axios";
import { useQuery } from "@tanstack/react-query";


export default function Home() {

  const { data: posts, isLoading: isLoadingPost } = useQuery({
    queryKey: ["posts"],
    queryFn: async () => {
      const response = await axios.get(`/api/posts`);
      return response.data;
    },
  });
  console.log(posts);


  if (isLoadingPost) {
    return (
      <div className="text-center">
        <span className="loading loading-spinner loading-lg"></span>
      </div>
    );
  }

  return (
    <main className="grid items-center justify-center md:grid-cols-2 lg:grid-cols-3 gap-4 mt-10">
      {posts.map((post: { id: string; title: string; content: string | null; tag: { id: string; name: string; }; }) => (
        <PostCard key={post.id} post={post} />
      ))}
    </main>
  );
}
