import prisma from "@/app/lib/prisma";
import { NextResponse } from "next/server";

export async function POST(req: Request) {
  try {
    const body = await req.json();
    console.log(body)
    const post = await prisma.post.create({
      data: { title: body.title, content: body.content, tagId: body.tagId },
    });
    return NextResponse.json(post, { status: 200 });
  } catch (error) {
    return NextResponse.json(
      { message: "could not create  post" },
      { status: 500 }
    );
  }
}

export async function GET() {
  try {
    const post = await prisma.post.findMany({
      select: {
        id: true,
        title: true,
        content: true,
        tag: true,
      },
      orderBy: {
        createdAt: "desc",
      },
    });
    return NextResponse.json(post, { status: 200 });
  } catch (error) {
    return NextResponse.json(
      { message: "could not fetch posts" },
      { status: 500 }
    );
  }
}

